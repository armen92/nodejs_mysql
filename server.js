import mysql from "mysql";
import express from "express";
import { appPort as port, dbConfig } from "./config.js";

// Create express app
const app = express();
// apply json middlware for request objects
app.use(express.json());

//Create database connection
const dbConnection = mysql.createConnection(dbConfig);

async function dbQuery(query) {
  /*
    Function for making query to db
    @query: sql query
    return: promise object
  */
  return new Promise((resolve, reject) => {
    dbConnection.query(query, (error, results, fields) => {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

async function getUser(req, res) {
  try {
    if (req.params.userId) {
      const data = await dbQuery(`select * from ${req.body.tablename} where id=${req.params.userId};`);
      if (data.length) {
        console.log(data.length, data, "data length and data <<<<<<<");
        res.send(JSON.stringify(data));
      } else {
        res.status(404);
        res.send(JSON.stringify(`NO SUCH USER WITH ID ${req.params.userId} IN DATABASE`));
        console.log(`NO SUCH USER WITH ID ${req.params.userId} IN DATABASE`)
      }
    } else {
      const data = await dbQuery(`select * from ${req.body.tablename};`);
      console.log(data.length, data, "data length and data <<<<<<<");
      res.send(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error, "ERROR");
  }
}

async function bulkCreateUser(req, res) {
  try {
    let values = "";
    for (let i = 0; i < req.body.length; i++) {
      if (i) {
        values += `,("${req.body[i].username}",` + `"${req.body[i].email}",` + `${req.body[i].age})`;
      } else {
        values += `("${req.body[i].username}",` + `"${req.body[i].email}",` + `${req.body[i].age})`;
      }
    }
    await dbQuery(
      `INSERT INTO users (username, email, age) VALUES ${values}`);
    res.status(201);
    res.send(JSON.stringify({ Rows: "created" }));
  } catch (error) {
    console.log(error, "ERROR");
  }
}

async function createUser(req, res) {
  try {
    await dbQuery(
      `INSERT INTO users (username, email, age) VALUES 
        ('${req.body.username}', '${req.body.email}', ${req.body.age});`
    );
    res.status(201);
    res.send(JSON.stringify({ Row: "created" }));
  } catch (error) {
    console.log(error, "ERROR");
  }
}

async function deleteUser(req, res) {
  try {
    const deleteQuery = await dbQuery(`DELETE FROM users WHERE id=${req.params.userId};`);
    res.status(200);
    res.send(JSON.stringify({ Row: "deleted" }));
  }
  catch (error) {
    console.log(error, "ERROR");
  }
}

async function bulkDeleteUser(req, res) {
  // delete multiple users with one request
  // returm http status code 204
  try {
    let values = "";
    for (let i = 0; i < req.body.length; i++) {
      if (i) {
        values += `,${req.body[i].userId}`
      } else {
        values += `${req.body[i].userId}`
      }
    }
    await dbQuery(`DELETE FROM users WHERE id IN (${values});`);
    res.status(200);
    res.send(JSON.stringify({ Row: "deleted" }));
  } catch (error) {
    console.log(error, "ERROR");
  }
}

async function updateUser(req, res) {
  // get updated user_id and fileds that will be updated and update user row
  // returm http status code 204
  try {
    let fields = "";
    if(reqBody.username){}

    // if (reqBody.username) {
    //   let query = await dbQuery(`update users set username="${reqBody.username}" where id=${userId}`);
    //   res.status(204);
    //   res.send(JSON.stringify({ Row: "updated" }));
    // };
    // if (reqBody.email) {
    //   console.log(reqBody.age);
    //   let query = await dbQuery(`update users set email="${reqBody.email}" where id=${userId}`);
    //   res.status(204);
    //   res.send(JSON.stringify({ Row: "updated" }));
    // };
  } catch (error) {
    console.log(error, "ERROR");
  }
}

//2
// Add posts CRUD api (Create Read Update Delete)
// each user has many posts

// router
app.get("/users", getUser);
app.get("/users/:userId", getUser);
app.post("/users", createUser);
app.post("/users-bulk-create", bulkCreateUser);
app.delete("/users/:userId", deleteUser);
app.delete("/users", bulkDeleteUser);
app.patch("/users/:userId", updateUser);
app.put("/users/:userId", updateUser);
// use test route to view your request body
app.post("/test", (req, res) => {
  console.log(Object.keys(req.body));
  console.log(Object.values(req.body));
  res.json({ requestBody: req.body });
});

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
