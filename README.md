# nodejs_mysql
Download the project
https://gitlab.com/grigor.poghosyan/nodejs_mysql.git
```
cd nodejs_mysql
npm install
npm start
```

### How install nodejs and npm
https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

You must change config.js and apply your database credentials.
For homework you have to modify server.js file

### We are using expressjs nodejs framework 
https://expressjs.com/

### Blog that can help you
https://time2hack.com/creating-rest-api-in-node-js-with-express-and-mysql/

### Url which contain basic .gitignore files
https://github.com/github/gitignore

### Learm about http status codes
https://www.restapitutorial.com/httpstatuscodes.html
