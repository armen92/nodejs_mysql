-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-05-25 14:36:34.027

-- tables
-- Table: albums
CREATE TABLE albums (
    id int NOT NULL,
    users_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: chat_rooms
CREATE TABLE chat_rooms (
    id int NOT NULL,
    messages_id int NOT NULL,
    chats_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: chats
CREATE TABLE chats (
    id int NOT NULL,
    users_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: messages
CREATE TABLE messages (
    id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: photo_comments
CREATE TABLE photo_comments (
    id int NOT NULL,
    photos_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: photos
CREATE TABLE photos (
    id int NOT NULL,
    albums_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: posts
CREATE TABLE posts (
    id int NOT NULL,
    content varchar(255) NULL,
    created timestamp NULL,
    users_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: posts_comments
CREATE TABLE posts_comments (
    id int NOT NULL,
    content varchar(255) NULL,
    created timestamp NULL,
    posts_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: users
CREATE TABLE users (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    age int NOT NULL,
    CONSTRAINT id PRIMARY KEY (id)
);

-- foreign keys
-- Reference: albums_users (table: albums)
ALTER TABLE albums ADD CONSTRAINT albums_users FOREIGN KEY albums_users (users_id)
    REFERENCES users (id);

-- Reference: chat_rooms_chats (table: chat_rooms)
ALTER TABLE chat_rooms ADD CONSTRAINT chat_rooms_chats FOREIGN KEY chat_rooms_chats (chats_id)
    REFERENCES chats (id);

-- Reference: chat_rooms_messages (table: chat_rooms)
ALTER TABLE chat_rooms ADD CONSTRAINT chat_rooms_messages FOREIGN KEY chat_rooms_messages (messages_id)
    REFERENCES messages (id);

-- Reference: chats_users (table: chats)
ALTER TABLE chats ADD CONSTRAINT chats_users FOREIGN KEY chats_users (users_id)
    REFERENCES users (id);

-- Reference: photo_comments_photos (table: photo_comments)
ALTER TABLE photo_comments ADD CONSTRAINT photo_comments_photos FOREIGN KEY photo_comments_photos (photos_id)
    REFERENCES photos (id);

-- Reference: photos_albums (table: photos)
ALTER TABLE photos ADD CONSTRAINT photos_albums FOREIGN KEY photos_albums (albums_id)
    REFERENCES albums (id);

-- Reference: posts_comments_posts (table: posts_comments)
ALTER TABLE posts_comments ADD CONSTRAINT posts_comments_posts FOREIGN KEY posts_comments_posts (posts_id)
    REFERENCES posts (id);

-- Reference: posts_users (table: posts)
ALTER TABLE posts ADD CONSTRAINT posts_users FOREIGN KEY posts_users (users_id)
    REFERENCES users (id);

-- End of file.

